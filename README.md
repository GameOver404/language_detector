# Language Detection
A class that provides language detection for a given text.

# Usage
An example on how to use the  `LangDetector` class can be found in `example.py`.

