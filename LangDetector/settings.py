#!/usr/bin/env python
# -*- coding: utf-8 -*-

# supported languages. this will be used
# to e.g. load the stopwords files
languages = ("de", "en")
