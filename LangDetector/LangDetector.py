#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import os
import re
import string

from langdetect import detect_langs
from langdetect.language import Language

from .Logger import Logger
from . import settings


class LangDetector(Logger):

    def __init__(self):
        super().__init__(__name__)
        self.logger.info("Initializing LangDetector.")

        # read stopwords for all languages
        self.stopwords = {}
        for lang in settings.languages:
            with codecs.open(os.path.join(os.path.dirname(__file__), "res/stopwords", lang), encoding = "utf-8") as f:
                self.stopwords[lang] = [l.strip() for l in f.readlines()]

    def _count_stopwords(self, text):
        """Counts stopwords that appear in text for all languages.
        """
        res = {}
        pattern = re.compile('[\W_]+', re.UNICODE)  # any non-word char (in unicode)
        words = set([pattern.sub('', tok) for tok in text.lower().split()])

        # count number of stopwords (per language) found in words
        for lang in ("en", "de"):
            res[lang] = len(words.intersection(self.stopwords[lang]))

        self.logger.debug("Stopword count: %s.", res)
        return res

    def _langdetect_all(self, text):
        """Return the language that langdetect deems the most likely."""
        highest = Language(None, -1)
        res = detect_langs(text)
        for item in res:
            if item.prob > highest.prob:
                highest = item
        return highest

    def detect_language(self, text):
        self.logger.debug("Trying to detect language for '%s[...]'.", text[:45])

        # get the language with the most stopword matches
        ratios = self._count_stopwords(text)
        l1 = max(ratios, key=ratios.get)

        # get the language that langdetect thinks 'text' is in
        l2 = self._langdetect_all(text)

        # language could NOT be detected by either method
        if l1 is None and l2.lang is None:
            lang = None

        # language was detected by exactly one of the methods
        elif l1 is None and l2.lang is not None:
            lang = l2.lang
        elif l1 is not None and l2.lang is None:
            lang = l1

        # same language was detected by both of the methods
        elif l1 == l2.lang:
            lang = l1

        # methods detected different languages
        elif l1 != l2.lang:
            lang = l2.lang  # we trust langdetect more

        self.logger.debug("Detected language '%s'.", lang)
        return lang
