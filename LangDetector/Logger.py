#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

loglevel = logging.INFO


class Logger:
    def __init__(self, name=None):
        if name is None:
            name = __name__

        self.logger = logging.getLogger(name)
        # check for presence of existing handler to avoid unnecessary creation of another
        if not self.logger.handlers:
            self.logger.setLevel(loglevel)

            # create formatter
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

            # create console handler, set level and formatter, add handler to logger
            ch = logging.StreamHandler()
            ch.setLevel(loglevel)
            ch.setFormatter(formatter)
            self.logger.addHandler(ch)

            # create file handler, set level and formatter, add handler to logger
            #fh = logging.FileHandler("LangDetector.log", mode="w")  # always overwrites file
            #fh.setLevel(loglevel)
            #fh.setFormatter(formatter)
            #self.logger.addHandler(fh)
