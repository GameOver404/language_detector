#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import codecs
import json
import sys

from LangDetector import LangDetector


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f",
        "--filename",
        required=True,
        help="The file to read the text from."
    )

    args = parser.parse_args()

    with codecs.open(args.filename, "r", "utf-8") as f:
        text = f.read()

    ld = LangDetector()
    res = ld.detect_language(text)

    print("Detected language: '{}'.".format(res))
